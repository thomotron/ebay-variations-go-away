// this is the code which will be injected into a given page...

(function() {
	var data = String(window.location);
	var isEbay = data.indexOf("ebay") != -1;
	var isQuery = data.indexOf("?") != -1;
	var itemsPerPage = data.indexOf("&_ipg=200") != -1;

	// Check if this is an eBay search page
	if (isEbay & isQuery) {
		// Set items per page to 200 if not already
		if (!itemsPerPage) window.href += "&_ipg=200";

		// Hide all listings with price variations
		// Determined by whether the price box has any child elements (i.e. the 'to' between prices)
		$('.s-item__price').filter(function() {return $(this).children().length > 0}).parents('.s-item').hide();
		console.log('All those annoying variable listings have been removed.');
	}
})();
